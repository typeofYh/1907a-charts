/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import React from 'react';
import ReactDOM from 'react-dom/client';
import "./style/common.less";
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);


