/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import "./style/layout.less"
import { useState, useEffect, useCallback } from "react"
import _ from "lodash"

const Layout = ({children, uiWidth = 1920, uiHeight = 1080}) => {
  const [style, setStyle] = useState({
    width: uiWidth,
    height: uiHeight,
    transform: `scale(1) translate(-50%, -50%)`
  })
  const setScale = useCallback(() => {
    const w = window.innerWidth / style.width;
    const h = window.innerHeight / style.height;
    setStyle({
      ...style,
      transform: `scale(${w}, ${h}) translate(-50%, -50%)`
    })
  }, [uiWidth, uiHeight, style]);
  useEffect(() => {
    setScale();
    const resizeFn = _.debounce(() => {
      setScale();
    }, 500)
    window.addEventListener('resize', resizeFn)
    return () => {
      window.removeEventListener('resize', resizeFn)
    }
  }, [])
  return (
    <div className={'content-layout'} style={style}>
      {children}
    </div>
  )
}

export default Layout;